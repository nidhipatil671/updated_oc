<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceStyle extends Model
{
    protected $fillable = [
        'serv_master_id',
        'name'
    ];
    protected $primaryKey = 'id';
    protected $table = 'service_style';


    protected function serviceMaster()
    {
        return $this->belongsTo('App\ServiceMaster');
    }
}
