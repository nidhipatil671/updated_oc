<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceGender extends Model
{
    protected $fillable = [
        'name'
    ];
    protected $primaryKey = 'id';
    protected $table = 'service_gender';
}
