<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceSize extends Model
{
    protected $fillable = [
        'serv_master_id',
        'name'
    ];
    protected $primaryKey = 'id';
    protected $table = 'service_size';


    protected function service_master()
    {
        return $this->belongsTo('App\ServiceMaster', 'serv_master_id' , 'id')->select(array('id', 'name'));
    }
}
