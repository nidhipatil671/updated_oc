<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientProfiles extends Model
{
    protected $table = 'client_profiles';
}
