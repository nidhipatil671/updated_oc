<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceMaster extends Model
{
    protected $fillable = [
        'serv_category_id',
        'name'
    ];
    protected $primaryKey = 'id';
    protected $table = 'service_master';


    protected function service_category()
    {
        return $this->belongsTo('App\ServiceCategory', 'serv_category_id' , 'id')->select(array('id', 'name'));
    }
}
