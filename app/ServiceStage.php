<?php
/**
 * Created by PhpStorm.
 * User: MAT006
 * Date: 07-09-2017
 * Time: 01:42 PM
 */

namespace App;
use Eloquent;

class ServiceStage extends Eloquent
{
	protected $fillable = [
		'name'
	];
	protected $primaryKey = 'id';
	protected $table = 'service_stage';
}