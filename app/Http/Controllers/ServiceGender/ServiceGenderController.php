<?php

namespace App\Http\Controllers\ServiceGender;

use App\Http\Controllers\Controller;
use App\ServiceGender;
use Illuminate\Http\Request;

class ServiceGenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("service_gender.index");
    }


    public function lists()
    {
        $service_gender = ServiceGender::get();
        $responce = ["data"=>$service_gender];
        return json_encode($responce);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("service_gender.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service_gender = new ServiceGender();
        $service_gender->name = $request->service_gender;
        $service_gender->save();

        return redirect(url('admin/service-gender/index'))->with('success','record Stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service_gender = ServiceGender::find($id);
        return view('service_gender.edit',compact('service_gender'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service_gender = ServiceGender::where('id', $id)->first();
        $service_gender->name = $request->service_gender;
        $service_gender->save();

        return redirect(url('admin/service-gender/index'))->with('success', 'record Updated successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $service_gender = ServiceGender::find($id)->delete();
        return redirect()->back()->with('success', 'record Deleted successfully');
    }
}
