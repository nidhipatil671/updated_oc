<?php

namespace App\Http\Controllers\ServiceStyle;
use App\Http\Controllers\Controller;
use App\ServiceMaster;
use App\ServiceStyle;
use App\ServiceType;
use Illuminate\Http\Request;
use App\Http\Requests\ServiceStyleRequest;
use Illuminate\Support\Facades\DB;

class ServiceStyleController extends Controller
{
    public function index()
    {
        return view("service_style.index");

    }

    public function lists()
    {
        $service = ServiceStyle::join('service_master','service_style.serv_master_id','service_master.id')
            ->get(['service_master.name as service_master_name','service_style.*']);
        // dd($service);
        $responce = ["data"=>$service];
        return json_encode($responce);

    }

    public function create()
    {
         //fetching dropdown list
        $service_style =ServiceMaster::get();
        //dd($service_style);
        return view("service_style.create",compact('service_style'));
    }

    //store service style
    public function store(Request $request){
    //dd($request);
        if(count($request->serv_master)>0){
            foreach($request->serv_master as $item){
                $service_style = new ServiceStyle();
                $service_style->serv_master_id = $item;
                $service_style->name = $request->service_style;
                $service_style->save();
            }

            return redirect(url('admin/service-style/index'))->with('success','record Stored successfully');
        }else{
            return redirect()->back();
        }

    }

    public function edit($id){

        $style = ServiceStyle::where('id',$id)->first();
        $master =ServiceMaster::get(['id','name']);

        //dd($master);
        return view('service_style.edit',compact('style','master'));
    }


    public function update(Request $request, $id)
    {

        $style = ServiceStyle::where('id', $id)->first();
        // dd($master);
        if ($style) {
            if (count($request->serv_master) > 0) {
                foreach ($request->serv_master as $item) {
                    $style->name = $request->service_style;
                    $style->serv_master_id = $item;
                    $style->save();
                }
                return redirect(url('admin/service-style/index'))->with('success', 'record Updated successfully');
            } else {
                return redirect()->back();
            }
        }

    }

    public function delete($id){


        $service = ServiceStyle::find($id)->delete();
      //dd($service);
        return redirect()->back()->with('success', 'record Deleted successfully');

    }

}