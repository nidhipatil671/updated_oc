<?php

namespace App\Http\Controllers\ServiceSize;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ServiceSize;
use App\ServiceMaster;

class ServiceSizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("service_size.index");
    }

    public function lists()
    {
        $service = ServiceSize::join('service_master','service_size.serv_master_id','service_master.id')
            ->get(['service_master.name as service_master_name','service_size.*']);
        // dd($service);
        $responce = ["data"=>$service];
        return json_encode($responce);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $service_size = ServiceMaster::get();
        return view("service_size.create",compact('service_size'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(count($request->serv_master)>0){
            foreach($request->serv_master as $item){
                $service_size = new ServiceSize();
                $service_size->serv_master_id = $item;
                $service_size->name = $request->service_size;
                $service_size->save();
            }

            return redirect(url('admin/service-size/index'))->with('success','Record Stored Successfully!');
        }else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service_size = ServiceSize::where('id',$id)->first();
        $master =ServiceMaster::get(['id','name']);

        //dd($master);
        return view('service_size.edit',compact('service_size','master'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service_size = ServiceSize::where('id', $id)->first();
        // dd($master);
        if ($service_size) {
            if (count($request->serv_master) > 0) {
                foreach ($request->serv_master as $item) {
                    $service_size->name = $request->service_size;
                    $service_size->serv_master_id = $item;
                    $service_size->save();
                }
                return redirect(url('admin/service-size/index'))->with('success', 'Record Updated Successfully!');
            } else {
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $service_size = ServiceSize::find($id)->delete();
        //dd($service);
        return redirect()->back()->with('success', 'Record Deleted Successfully!');
    }
}
