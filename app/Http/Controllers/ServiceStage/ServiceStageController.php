<?php

namespace App\Http\Controllers\ServiceStage;

use App\Http\Controllers\Controller;
use App\ServiceMaster;
use App\ServiceStage;
use Illuminate\Http\Request;

class ServiceStageController extends Controller
{


    public function index()
    {
        return view("service_stage.index");

    }

    public function lists()
    {
        $service_stage = ServiceStage::get();
        $responce = ["data"=>$service_stage];
        return json_encode($responce);

    }

    public function create()
    {
        return view("service_stage.create");
    }


    public function store(Request $request){

                $service_stage = new ServiceStage();
                $service_stage->name = $request->service_stage;
                $service_stage->save();

            return redirect(url('admin/service-stage/index'))->with('success','record Stored successfully');

    }

    public function edit($id){

        $service_stage = ServiceStage::find($id);
        return view('service_stage.edit',compact('service_stage'));
    }


    public function update(Request $request, $id)
    {

        $service_stage = ServiceStage::where('id', $id)->first();
        $service_stage->name = $request->service_stage;
        $service_stage->save();

        return redirect(url('admin/service-stage/index'))->with('success', 'record Updated successfully');


    }

    public function delete($id){


        $service_stage = ServiceStage::find($id)->delete();
        return redirect()->back()->with('success', 'record Deleted successfully');

    }
}
