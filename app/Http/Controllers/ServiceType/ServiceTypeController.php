<?php

namespace App\Http\Controllers\ServiceType;

use App\Http\Controllers\Controller;
use App\ServiceMaster;
use App\ServiceType;
use Illuminate\Http\Request;

class ServiceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("service_type.index");
    }

    public function lists()
    {
        $servicetype = ServiceType::join('service_master','service_type.serv_master_id','service_master.id')
            ->get(['service_master.name as service_master_name','service_type.*']);
       //dd($servicetype);
        $responce = ["data"=>$servicetype];
        return json_encode($responce);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $service_type =ServiceMaster::get();
       // dd($service_type);
        return view("service_type.create",compact('service_type'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(count($request->serv_master)>0){
            foreach($request->serv_master as $item){
                $service_type = new ServiceType();
                $service_type->serv_master_id = $item;
                $service_type->name = $request->service_type;
                $service_type->save();
            }

            return redirect(url('admin/service-type/index'))->with('success','record stored successfully');
        }else{
            return redirect()->back();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service_type = ServiceType::where('id',$id)->first();
        $master =ServiceMaster::get(['id','name']);

        //dd($master);
        return view('service_type.edit',compact('service_type','master'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service_type = ServiceType::where('id', $id)->first();
        // dd($master);
        if ($service_type) {
            if (count($request->serv_master) > 0) {
                foreach ($request->serv_master as $item) {
                    $service_type->name = $request->service_type;
                    $service_type->serv_master_id = $item;
                    $service_type->save();
                }
                return redirect(url('admin/service-type/index'))->with('success', 'record Updated successfully');
            } else {
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $service_type = ServiceType::find($id)->delete();
        //dd($service);
        return redirect()->back()->with('success', 'record Deleted successfully');
    }
}
