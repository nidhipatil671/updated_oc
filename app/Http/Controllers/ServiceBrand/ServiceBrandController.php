<?php

namespace App\Http\Controllers\ServiceBrand;

use App\Http\Controllers\Controller;
use App\ServiceBrand;
use App\ServiceMaster;
use Illuminate\Http\Request;

class ServiceBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("service_brand.index");
    }


    public function lists()
    {
        $service_brand = ServiceBrand::join('service_master','service_brand.serv_master_id','service_master.id')
            ->get(['service_master.name as service_master_name','service_brand.*']);

        $responce = ["data"=>$service_brand];
        //dd($responce);
        return json_encode($responce);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //fetching dropdown list
        $service_brand =ServiceMaster::get();
        //dd($service_brand);
        return view("service_brand.create",compact('service_brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(count($request->serv_master)>0){
            foreach($request->serv_master as $item){
                $service_brand = new ServiceBrand();
                $service_brand->serv_master_id = $item;
                $service_brand->name = $request->service_brand;
                $service_brand->save();
            }

            return redirect(url('admin/service-brand/index'))->with('success','record Stored successfully');
        }else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service_brand = ServiceBrand::where('id',$id)->first();
        $master =ServiceMaster::get(['id','name']);

        //dd($master);
        return view('service_brand.edit',compact('service_brand','master'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service_brand = ServiceBrand::where('id', $id)->first();
        // dd($master);
        if ($service_brand) {
            if (count($request->serv_master) > 0) {
                foreach ($request->serv_master as $item) {
                    $service_brand->name = $request->service_brand;
                    $service_brand->serv_master_id = $item;
                    $service_brand->save();
                }
                return redirect(url('admin/service-brand/index'))->with('success', 'record Updated successfully');
            } else {
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id){


        $service_brand = ServiceBrand::find($id)->delete();
        //dd($service);
        return redirect()->back()->with('success', 'record Deleted successfully');

    }
}
