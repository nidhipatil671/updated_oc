<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ClientProfiles;
use App\Clients;
use App\Locations;
use App\Company;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("clients.index");
    }

    public function lists()
    {
        // $clients = ClientProfiles::get();
        // $responce = ["data"=>$clients];
        // return json_encode($responce);


        $clients = ClientProfiles::join('company','client_profiles.company_id','company.id')
            ->join('locations','client_profiles.location_id','locations.id')
            ->get(['company.name as company','locations.display_name as location', 
                'client_profiles.*']);
        $responce = ["data"=>$clients];
        return json_encode($responce);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("clients.create");
    }

    public function view($id)
    {
        $clients = ClientProfiles::where('id','=',$id)->get();
        return view('clients.view')->with('clients', $clients);
        // return view("clients.view");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
