<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::group(['prefix'=>'admin','as'=>'admin.'],function(){
    Route::group(['prefix'=>'service-style','namespace'=>'ServiceStyle','as'=>'service-style.'],function(){
        Route::get('index','ServiceStyleController@index');
        Route::get('lists','ServiceStyleController@lists')->name('lists');
        Route::get('create','ServiceStyleController@create');
        Route::get('store','ServiceStyleController@store');
        Route::any('/edit/{id}','ServiceStyleController@edit');
        Route::get('delete/{id}','ServiceStyleController@delete');
        Route::post('update/{id}','ServiceStyleController@update');
    });


    Route::group(['prefix'=>'service-type','namespace'=>'ServiceType','as'=>'service-type.'],function(){
        Route::get('index','ServiceTypeController@index');
        Route::get('lists','ServiceTypeController@lists')->name('lists');
        Route::get('create','ServiceTypeController@create');
        Route::get('store','ServiceTypeController@store');
        Route::any('/edit/{id}','ServiceTypeController@edit');
        Route::get('delete/{id}','ServiceTypeController@delete');
        Route::post('update/{id}','ServiceTypeController@update');
    });

    Route::group(['prefix'=>'service-brand','namespace'=>'ServiceBrand','as'=>'service-brand.'],function(){
        Route::get('index','ServiceBrandController@index');
        Route::get('lists','ServiceBrandController@lists')->name('lists');
        Route::get('create','ServiceBrandController@create');
        Route::get('store','ServiceBrandController@store');
        Route::any('/edit/{id}','ServiceBrandController@edit');
        Route::get('delete/{id}','ServiceBrandController@delete');
        Route::post('update/{id}','ServiceBrandController@update');
    });

    Route::group(['prefix'=>'service-stage','namespace'=>'ServiceStage','as'=>'service-stage.'],function(){
        Route::get('index','ServiceStageController@index');
        Route::get('lists','ServiceStageController@lists')->name('lists');
        Route::get('create','ServiceStageController@create');
        Route::get('store','ServiceStageController@store');
        Route::any('/edit/{id}','ServiceStageController@edit');
        Route::get('delete/{id}','ServiceStageController@delete');
        Route::post('update/{id}','ServiceStageController@update');
    });

    Route::group(['prefix'=>'service-gender','namespace'=>'ServiceGender','as'=>'service-gender.'],function(){
        Route::get('index','ServiceGenderController@index');
        Route::get('lists','ServiceGenderController@lists')->name('lists');
        Route::get('create','ServiceGenderController@create');
        Route::get('store','ServiceGenderController@store');
        Route::any('/edit/{id}','ServiceGenderController@edit');
        Route::get('delete/{id}','ServiceGenderController@delete');
        Route::post('update/{id}','ServiceGenderController@update');
    });

    Route::group(['prefix'=>'service-size','namespace'=>'ServiceSize','as'=>'service-size.'],function(){
        Route::get('index','ServiceSizeController@index');
        Route::get('lists','ServiceSizeController@lists')->name('lists');
        Route::get('create','ServiceSizeController@create');
        Route::get('store','ServiceSizeController@store');
        Route::any('/edit/{id}','ServiceSizeController@edit');
        Route::get('delete/{id}','ServiceSizeController@delete');
        Route::post('update/{id}','ServiceSizeController@update');
    });

    Route::group(['prefix'=>'employee','namespace'=>'Employee','as'=>'employee.'],function(){
        Route::get('index','EmployeeController@index');
        Route::get('create','EmployeeController@create');
        Route::get('employee_filter','EmployeeController@employee_filter');
        Route::get('employee_report','EmployeeController@employee_report');
    });

    Route::group(['prefix'=>'client','namespace'=>'Clients','as'=>'client.'],function(){
        Route::get('index','ClientsController@index');
        Route::get('lists','ClientsController@lists')->name('lists');
        Route::get('create','ClientsController@create');
        Route::get('store','ClientsController@store');
        Route::any('/edit/{id}','ClientsController@edit');
        Route::get('delete/{id}','ClientsController@delete');
        Route::post('update/{id}','ClientsController@update');
        Route::get('view/{id}','ClientsController@view');
    });

});



