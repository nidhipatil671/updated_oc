
@extends('layouts.default')
@section('css')


@stop
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid"></div>

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">

            <div class="kt-portlet" >
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h4 class="kt-portlet__head-title">
                            <h5> <strong>{!! trans('messages.add_new') !!}</strong> {!! trans('messages.client') !!}</h5>
                        </h4>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="{!! url('admin/client/store') !!}" id="client">
                    <div class="kt-portlet__body">

                        <div class="row">
                            <div class="form-group col-6">
                                <label>{!! trans('messages.first').' '.trans('messages.name') !!}</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="{{trans('messages.first').' '.trans('messages.name')}}">
                            </div>
                            <div class="form-group col-6">
                                <label>{!! trans('messages.last').' '.trans('messages.name') !!}</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="{{trans('messages.last').' '.trans('messages.name')}}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-6">
                                <label>{!! trans('messages.mobile')!!}</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="{{trans('messages.mobile')}}">
                            </div>
                            <div class="form-group col-6">
                                <label>{!! trans('messages.gender')!!}</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="{{trans('messages.gender')}}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-6">
                                <label>{!! trans('messages.email')!!}</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="{{trans('messages.email')}}">
                            </div>
                            <div class="form-group col-6">
                                <label>{!! trans('messages.marital')!!}</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="{{trans('messages.marital')}}">
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-6">
                                <label>{!! trans('messages.company')!!}</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="{{trans('messages.company')}}">
                            </div>
                            <div class="form-group col-6">
                                <label>{!! trans('messages.location')!!}</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="{{trans('messages.location')}}">
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-primary">{!! trans('messages.create').' '.trans('messages.client') !!}</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>

        </div>
    </div>
@stop

@section('script')
    <script>
        "use strict";
        // Class definition

        var KTFormControls = function () {
            // Private functions

            var demo1 = function () {
                $( "#service" ).validate({
                    // define validation rules
                    rules: {
                        input: {
                            required: true
                        },

                        option: {
                            required: true
                        },

                    },
                    messages: {
                        input:{
                            required: "Please enter service style",
                        },
                        option: {
                            required: "Please select options",
                        },
                    },

                    //display error alert on form submit
                    invalidHandler: function(event, validator) {
                        var alert = $('#service_msg');
                        alert.parent().removeClass('kt-hidden');
                        KTUtil.scrollTo("service", -200);
                    },

                    submitHandler: function (form) {
                        //form[0].submit(); // submit the form
                        form.submit();
                    }
                });
            }

            return {
                // public functions
                init: function() {
                    demo1();
                }
            };
        }();

        $('#submit').click(function() {
            $("#service").valid();
        });

        jQuery(document).ready(function() {
            KTFormControls.init();
        });
    </script>

@stop


