<!DOCTYPE html>

<!--
Theme: Keen - The Ultimate Bootstrap Admin Theme
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: You must have a valid license purchased only from https://themes.getbootstrap.com/product/keen-the-ultimate-bootstrap-admin-theme/ in order to legally use the theme for your project.
-->
<html lang="en">

<!-- begin::Head -->
@include('layouts.head')
<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Header Mobile -->
@include('layouts.mobile_header')
<!-- end:: Header Mobile -->

<!-- begin:: Root -->
<div class="kt-grid kt-grid--hor kt-grid--root">

    <!-- begin:: Page -->
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <!-- begin:: Aside -->
        @include('layouts.sidebar')
        <!-- end:: Aside -->

        <!-- begin:: Wrapper -->
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <!-- begin:: Header -->
            @include('layouts.header')
            <!-- end:: Header -->
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                    <div class="alert alert-light alert-elevate" role="alert">
                        <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                        <div class="alert-text">
                            Each column has an optional rendering control called columns.render which can be used to process the content of each cell before the data is used.
                            See official documentation <a class="kt-link kt-font-bold" href="https://datatables.net/examples/advanced_init/column_render.html" target="_blank">here</a>.
                        </div>
                    </div>
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Column Rendering
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                                <thead>
                                <tr>
                                    <th>Record ID</th>
                                    <th>Company Email</th>
                                    <th>Company Agent</th>
                                    <th>Company Name</th>
                                    <th>Status</th>
                                    <th>Type</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>hboule0@hp.com</td>
                                    <td>Hayes Boule</td>
                                    <td>Casper-Kerluke</td>
                                    <td>5</td>
                                    <td>1</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>hbresnen1@theguardian.com</td>
                                    <td>Humbert Bresnen</td>
                                    <td>Hodkiewicz and Sons</td>
                                    <td>2</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>jlabro2@kickstarter.com</td>
                                    <td>Jareb Labro</td>
                                    <td>Kuhlman Inc</td>
                                    <td>6</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>ktosspell3@flickr.com</td>
                                    <td>Krishnah Tosspell</td>
                                    <td>Prosacco-Kessler</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>dkernan4@mapquest.com</td>
                                    <td>Dale Kernan</td>
                                    <td>Bernier and Sons</td>
                                    <td>5</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>hbentham5@nih.gov</td>
                                    <td>Halley Bentham</td>
                                    <td>Schoen-Metz</td>
                                    <td>1</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>bpenddreth6@example.com</td>
                                    <td>Burgess Penddreth</td>
                                    <td>DuBuque, Stanton and Stanton</td>
                                    <td>5</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>csedwick7@wikispaces.com</td>
                                    <td>Cob Sedwick</td>
                                    <td>Homenick-Nolan</td>
                                    <td>3</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>tcallaghan8@squidoo.com</td>
                                    <td>Tabby Callaghan</td>
                                    <td>Daugherty-Considine</td>
                                    <td>2</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>bjarry9@craigslist.org</td>
                                    <td>Broddy Jarry</td>
                                    <td>Walter Group</td>
                                    <td>1</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>11</td>
                                    <td>mmcgougana@dion.ne.jp</td>
                                    <td>Marjorie McGougan</td>
                                    <td>Littel and Sons</td>
                                    <td>6</td>
                                    <td>1</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>12</td>
                                    <td>espriggingb@china.com.cn</td>
                                    <td>Edsel Sprigging</td>
                                    <td>Kulas, Huels and Strosin</td>
                                    <td>6</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>13</td>
                                    <td>jgouldebyc@cocolog-nifty.com</td>
                                    <td>Jess Gouldeby</td>
                                    <td>Moen Group</td>
                                    <td>5</td>
                                    <td>1</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>14</td>
                                    <td>mmatzld@msn.com</td>
                                    <td>Marys Matzl</td>
                                    <td>Emard-Gerhold</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>15</td>
                                    <td>gfranscionie@craigslist.org</td>
                                    <td>Gabrila Franscioni</td>
                                    <td>Gusikowski LLC</td>
                                    <td>4</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>16</td>
                                    <td>cbookerf@blogs.com</td>
                                    <td>Cozmo Booker</td>
                                    <td>Dickinson-Klein</td>
                                    <td>1</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>17</td>
                                    <td>alarkingg@elegantthemes.com</td>
                                    <td>Arlie Larking</td>
                                    <td>Rosenbaum Group</td>
                                    <td>4</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>18</td>
                                    <td>yscogingsh@liveinternet.ru</td>
                                    <td>Yorker Scogings</td>
                                    <td>Gorczany LLC</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>19</td>
                                    <td>dmuscotti@bloomberg.com</td>
                                    <td>Dominick Muscott</td>
                                    <td>Swaniawski-Sipes</td>
                                    <td>2</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>20</td>
                                    <td>lkynforthj@meetup.com</td>
                                    <td>Laurette Kynforth</td>
                                    <td>Torp-Satterfield</td>
                                    <td>1</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>21</td>
                                    <td>blycettk@t.co</td>
                                    <td>Beryl Lycett</td>
                                    <td>Schoen Inc</td>
                                    <td>3</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>22</td>
                                    <td>cboggasl@quantcast.com</td>
                                    <td>Carny Boggas</td>
                                    <td>Kuphal LLC</td>
                                    <td>2</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>23</td>
                                    <td>daxelbym@about.me</td>
                                    <td>Dyana Axelby</td>
                                    <td>Runolfsdottir-Hayes</td>
                                    <td>2</td>
                                    <td>1</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>24</td>
                                    <td>oduffyn@de.vu</td>
                                    <td>Orelle Duffy</td>
                                    <td>Roberts and Sons</td>
                                    <td>5</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>25</td>
                                    <td>tkindero@hud.gov</td>
                                    <td>Taylor Kinder</td>
                                    <td>Terry-Howell</td>
                                    <td>3</td>
                                    <td>1</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>26</td>
                                    <td>eaylesburyp@va.gov</td>
                                    <td>Emanuele Aylesbury</td>
                                    <td>Torp LLC</td>
                                    <td>3</td>
                                    <td>1</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>27</td>
                                    <td>dgibkeq@multiply.com</td>
                                    <td>Dorie Gibke</td>
                                    <td>Tremblay and Sons</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>28</td>
                                    <td>mharraginr@arstechnica.com</td>
                                    <td>Melisandra Harragin</td>
                                    <td>Turner-Cartwright</td>
                                    <td>5</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>29</td>
                                    <td>blampetts@behance.net</td>
                                    <td>Berenice Lampett</td>
                                    <td>Johnston-Fritsch</td>
                                    <td>2</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>30</td>
                                    <td>tmcmurthyt@psu.edu</td>
                                    <td>Tammie McMurthy</td>
                                    <td>Sipes, Conn and Stiedemann</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>31</td>
                                    <td>djoyesu@microsoft.com</td>
                                    <td>Dinnie Joyes</td>
                                    <td>Keebler Group</td>
                                    <td>5</td>
                                    <td>1</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>32</td>
                                    <td>kaxelbeyv@macromedia.com</td>
                                    <td>Kerianne Axelbey</td>
                                    <td>Wolff, Sporer and Bechtelar</td>
                                    <td>6</td>
                                    <td>1</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>33</td>
                                    <td>kmacterlaghw@dailymotion.com</td>
                                    <td>Kiley MacTerlagh</td>
                                    <td>Hauck Inc</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>34</td>
                                    <td>tshuttlex@washingtonpost.com</td>
                                    <td>Trula Shuttle</td>
                                    <td>Will-Morissette</td>
                                    <td>5</td>
                                    <td>1</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>35</td>
                                    <td>hbrisleny@4shared.com</td>
                                    <td>Hollis Brislen</td>
                                    <td>Lowe, Jaskolski and Gulgowski</td>
                                    <td>4</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>36</td>
                                    <td>mbattinz@gov.uk</td>
                                    <td>Marsh Battin</td>
                                    <td>Fay LLC</td>
                                    <td>6</td>
                                    <td>1</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>37</td>
                                    <td>ppinnion10@state.tx.us</td>
                                    <td>Patrizio Pinnion</td>
                                    <td>Haag-Stokes</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>38</td>
                                    <td>idaouse11@yolasite.com</td>
                                    <td>Ilario Daouse</td>
                                    <td>Nitzsche, Davis and Romaguera</td>
                                    <td>3</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>39</td>
                                    <td>bcoleborn12@upenn.edu</td>
                                    <td>Blisse Coleborn</td>
                                    <td>Bailey, Windler and Marquardt</td>
                                    <td>6</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>40</td>
                                    <td>ajouannisson13@issuu.com</td>
                                    <td>Augustin Jouannisson</td>
                                    <td>Witting, Reilly and Morar</td>
                                    <td>3</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>41</td>
                                    <td>kjennison14@slashdot.org</td>
                                    <td>Kaleena Jennison</td>
                                    <td>Johnston Inc</td>
                                    <td>5</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>42</td>
                                    <td>mpetronis15@bandcamp.com</td>
                                    <td>Mariel Petronis</td>
                                    <td>Mitchell, Bashirian and Schroeder</td>
                                    <td>5</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>43</td>
                                    <td>ascroggie16@youku.com</td>
                                    <td>Adamo Scroggie</td>
                                    <td>Cartwright Group</td>
                                    <td>4</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>44</td>
                                    <td>lkilmartin17@bigcartel.com</td>
                                    <td>Lewiss Kilmartin</td>
                                    <td>Stroman-Orn</td>
                                    <td>3</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>45</td>
                                    <td>csachno18@blogs.com</td>
                                    <td>Claretta Sachno</td>
                                    <td>Zemlak-Cruickshank</td>
                                    <td>4</td>
                                    <td>1</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>46</td>
                                    <td>bvan19@ebay.co.uk</td>
                                    <td>Bryn Van Castele</td>
                                    <td>Beier-Mante</td>
                                    <td>5</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>47</td>
                                    <td>tgatch1a@4shared.com</td>
                                    <td>Tades Gatch</td>
                                    <td>Klocko, Koelpin and Nikolaus</td>
                                    <td>5</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>48</td>
                                    <td>rjolland1b@artisteer.com</td>
                                    <td>Reinold Jolland</td>
                                    <td>Zieme-Funk</td>
                                    <td>4</td>
                                    <td>2</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>49</td>
                                    <td>kbrainsby1c@hibu.com</td>
                                    <td>Ky Brainsby</td>
                                    <td>Towne Inc</td>
                                    <td>2</td>
                                    <td>3</td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td>50</td>
                                    <td>sgiddings1d@samsung.com</td>
                                    <td>Sheryl Giddings</td>
                                    <td>Grimes, Ryan and Larkin</td>
                                    <td>3</td>
                                    <td>1</td>
                                    <td nowrap></td>
                                </tr>
                                </tbody>
                            </table>

                            <!--end: Datatable -->
                        </div>
                    </div>
                </div>

                <!-- end:: Content -->
            </div>
            <div class="kt_datatable" id="kt_datatable">hiiiiiiiiiiiiiiiiiii</div>
            <script>

                var datatable = $('.kt_datatable').KTDatatable({
                    // datasource definition
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                url: 'inc/api/datatables/demos/default.php',
                                map: function(raw) {
                                    // sample data mapping
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    return dataSet;
                                },
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                    },

                    // layout definition
                    layout: {
                        scroll: false,
                        footer: false,
                    },

                    // column sorting
                    sortable: true,

                    pagination: true,

                    search: {
                        input: $('#generalSearch'),
                    },

                    // columns definition
                    columns: [
                        {
                            field: 'id',
                            title: '#',
                            sortable: 'asc',
                            width: 40,
                            type: 'number',
                            selector: false,
                            textAlign: 'center',
                        }, {
                            field: 'employee_id',
                            title: 'Employee ID',
                        }, {
                            field: 'name',
                            title: 'Name',
                            template: function(row, index, datatable) {
                                return row.first_name + ' ' + row.last_name;
                            },
                        }, {
                            field: 'email',
                            width: 150,
                            title: 'Email',
                        }, {
                            field: 'phone',
                            title: 'Phone',
                        }, {
                            field: 'hire_date',
                            title: 'Hire Date',
                            type: 'date',
                            format: 'MM/DD/YYYY',
                        }, {
                            field: 'gender',
                            title: 'Gender',
                        }, {
                            field: 'status',
                            title: 'Status',
                            // callback function support for column rendering
                            template: function(row) {
                                var status = {
                                    1: {'title': 'Pending', 'class': 'kt-badge--brand'},
                                    2: {'title': 'Delivered', 'class': ' kt-badge--metal'},
                                    3: {'title': 'Canceled', 'class': ' kt-badge--primary'},
                                    4: {'title': 'Success', 'class': ' kt-badge--success'},
                                    5: {'title': 'Info', 'class': ' kt-badge--info'},
                                    6: {'title': 'Danger', 'class': ' kt-badge--danger'},
                                    7: {'title': 'Warning', 'class': ' kt-badge--warning'},
                                };
                                return '<span class="kt-badge ' + status[row.status].class + ' kt-badge--inline kt-badge--pill">' + status[row.status].title + '</span>';
                            },
                        }, {
                            field: 'type',
                            title: 'Type',
                            // callback function support for column rendering
                            template: function(row) {
                                var status = {
                                    1: {'title': 'Online', 'state': 'danger'},
                                    2: {'title': 'Retail', 'state': 'primary'},
                                    3: {'title': 'Direct', 'state': 'accent'},
                                };
                                return '<span class="kt-badge kt-badge--' + status[row.type].state + ' kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-' + status[row.type].state + '">' +
                                        status[row.type].title + '</span>';
                            },
                        }, {
                            field: 'Actions',
                            title: 'Actions',
                            sortable: false,
                            width: 130,
                            overflow: 'visible',
                            textAlign: 'center',
                            template: function(row, index, datatable) {
                                var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                                return '<div class="dropdown ' + dropup + '">\
                        <a href="#" class="btn btn-hover-brand btn-icon btn-pill" data-toggle="dropdown">\
                            <i class="la la-ellipsis-h"></i>\
                        </a>\
                        <div class="dropdown-menu dropdown-menu-right">\
                            <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>\
                            <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>\
                            <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>\
                        </div>\
                    </div>\
                    <a href="#" class="btn btn-hover-brand btn-icon btn-pill" title="Edit details">\
                        <i class="la la-edit"></i>\
                    </a>\
                    <a href="#" class="btn btn-hover-danger btn-icon btn-pill" title="Delete">\
                        <i class="la la-trash"></i>\
                    </a>';
                            },
                        }],

                });

            </script>
            <!-- begin:: Footer -->
            @include('layouts.footer')
            <!-- end:: Footer -->
        </div>

        <!-- end:: Wrapper -->
    </div>

    <!-- end:: Page -->
</div>

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>



@include('layouts.script')
<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>