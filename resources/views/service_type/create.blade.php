
@extends('layouts.default')
@section('css')


@stop
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid"></div>

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">

            <div class="kt-portlet" >
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Add New Service Type</h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form"  style="" action="{!! url('admin/service-type/store') !!}" id="service" >
                    {{ csrf_field() }}
                    <div class="kt-portlet__body">

                        <div class="form-group row validated">
                            <label class="col-form-label col-lg-3" for="inputSuccess1">{!! trans('messages.service_type') !!}</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" id="inputSuccess1" name="service_type" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">{!! trans('messages.service_master') !!}</label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <option value="serv_master" ></option>
                                <select class="form-control" id="kt_bootstrap_select"  multiple name="serv_master[]" required>

                                    @foreach ($service_type as $servmaster)
                                        <option value="{{ $servmaster->id }}">{{ $servmaster->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-9 ml-lg-auto">
                                    <button type="submit" class="btn btn-brand" id="submit">Submit</button>
                                    <a href="{{url()->previous()}}" type="button" class="btn btn-secondary">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>

        </div>
    </div>
@stop

@section('script')
    <script>
        "use strict";
        // Class definition

        var KTFormControls = function () {
            // Private functions

            var demo1 = function () {
                $( "#service" ).validate({
                    // define validation rules
                    rules: {
                        input: {
                            required: true
                        },

                        option: {
                            required: true
                        },

                    },
                    messages: {
                        input:{
                            required: "Please enter service type",
                        },
                        option: {
                            required: "Please select options",
                        },
                    },

                    //display error alert on form submit
                    invalidHandler: function(event, validator) {
                        var alert = $('#service_msg');
                        alert.parent().removeClass('kt-hidden');
                        KTUtil.scrollTo("service", -200);
                    },

                    submitHandler: function (form) {
                        //form[0].submit(); // submit the form
                        form.submit();
                    }
                });
            }

            return {
                // public functions
                init: function() {
                    demo1();
                }
            };
        }();

        $('#submit').click(function() {
            $("#service").valid();
        });

        jQuery(document).ready(function() {
            KTFormControls.init();
        });
    </script>

@stop


