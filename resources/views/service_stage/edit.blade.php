
@extends('layouts.default')
@section('css')


@stop
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid"></div>

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Edit Service Stage

                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" style="" action="{!! url('admin/service-stage/update',[$service_stage->id]) !!}" method="post">
                    {{ csrf_field() }}
                    <div class="kt-portlet__body">
                        <div class="form-group row validated">
                            <label class="col-form-label col-lg-3" for="inputSuccess1" >{!! trans('messages.service_stage') !!}</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" id="inputSuccess1" value="{{  $service_stage->name }}" name="service_stage" >

                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-9 ml-lg-auto">
                                    <button type="submit" class="btn btn-brand">Submit</button>
                                    <a href="{{url()->previous()}}" type="button" class="btn btn-secondary">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>


            <div class="kt-portlet__body kt-portlet__body--fit">

                <!--begin: Datatable -->
                <div class="kt_datatable" id="base_column_width"></div>

                <!--end: Datatable -->
            </div>
        </div>
    </div>
@stop

@section('script')


@stop


