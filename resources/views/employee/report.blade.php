
@extends('layouts.default')
@section('css')


@stop
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid"></div>

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">

            <div class="kt-portlet" >
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h4 class="kt-portlet__head-title">
                            <h5> <strong>{!! trans('messages.filter') !!}</strong> </h5>
                        </h4>
                    </div>
                </div>

                <!--begin::Form-->

                <form class="kt-form" action="{!! url('admin/report/store') !!}" id="employee">
                    <div class="kt-portlet__body">

                        <div class="row">
                            <div class="form-group col-6">
                                <label class="col-form-label col-lg-3 col-sm-12">{!! trans('messages.duration') !!}</label>
                                <select class="form-control kt-select2" id="kt_select2_5" name="param">
                                    <option value="2" selected="selected">{!! trans('messages.select_one') !!}</option>
                                </select>
                            </div>
                            <div class="form-group col-6">
                                <label class="col-form-label col-lg-3 col-sm-12">{!! trans('messages.type') !!}</label>
                                <select class="form-control kt-select2" id="kt_select2_5" name="param">
                                    <option value="2" selected="selected">{!! trans('messages.select_one') !!}</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-6">
                                <label class="col-form-label col-lg-3 col-sm-12">{!! trans('messages.role') !!}</label>
                                <select class="form-control kt-select2" id="kt_select2_5" name="param">
                                    <option value="2" selected="selected">{!! trans('messages.select_one') !!}</option>
                                </select>
                            </div>
                            <div class="form-group col-6">
                                <label class="col-form-label col-lg-3 col-sm-12">{!! trans('messages.designation') !!}</label>
                                <select class="form-control kt-select2" id="kt_select2_5" name="param">
                                    <option value="2" selected="selected">{!! trans('messages.select_one') !!}</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-6">
                                <label class="col-form-label col-lg-3 col-sm-12">{!! trans('messages.location') !!}</label>
                                <select class="form-control kt-select2" id="kt_select2_5" name="param">
                                    <option value="2" selected="selected">{!! trans('messages.select_one') !!}</option>
                                </select>
                            </div>
                            <div class="form-group col-6">
                                <label class="col-form-label col-lg-3 col-sm-12">{!! trans('messages.status') !!}</label>
                                <select class="form-control kt-select2" id="kt_select2_5" name="param">
                                    <option value="2" selected="selected">{!! trans('messages.select_one') !!}</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-6">
                                <label class="col-form-label col-lg-3 col-sm-12">{!! trans('messages.gender') !!}</label>
                                <select class="form-control kt-select2" id="kt_select2_5" name="param">
                                    <option value="2" selected="selected">{!! trans('messages.select_one') !!}</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-primary">{!! trans('messages.filter') !!}</button>
                            <a href="{{url()->previous()}}" type="button" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

        </div>
    </div>
@stop

@section('script')


@stop


