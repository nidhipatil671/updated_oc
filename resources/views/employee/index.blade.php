
@extends('layouts.default')
@section('css')


@stop
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        </div>
        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h4 class="kt-portlet__head-title">
                            <h5><strong>{!! trans('messages.list_all') !!}</strong> {!! trans('messages.user') !!}</h5>
                        </h4>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="dropdown dropdown-inline" data-toggle="kt-tooltip"  data-placement="top">
                                <a href="{!! url('admin/employee/employee_filter') !!}"  class="btn btn btn-label btn-label-brand btn-bold" data-toggle="dropdown" data-offset="0 0" aria-haspopup="true" aria-expanded="false">
                                    Filter
                                </a>

                            </div>
                            &nbsp;
                            <div class="dropdown dropdown-inline" data-toggle="kt-tooltip"  data-placement="top">
                                <a href="#" class="btn btn btn-label btn-label-brand btn-bold" data-toggle="dropdown" data-offset="0 0" aria-haspopup="true" aria-expanded="false">
                                    Reports
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="kt-nav kt-nav--active-bg" id="kt_nav_1" role="tablist">
                                        <li class="kt-nav__item">
                                            <a href="{!! url('admin/employee/employee_filter') !!}" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-psd"></i>
                                                <span class="kt-nav__link-text">Employment Report</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            &nbsp;
                            <a href="{!! url('admin/employee/create') !!}" class="btn btn-brand btn-bold btn-upper btn-font-sm">
                                <i class="la la-plus"></i>
                                {!! trans('messages.add_new') !!}
                            </a>
                        </div>
                    </div>
                </div>


                <div class="kt-portlet__body">

                    <!--begin: Search Form -->
                    <div class="kt-form kt-fork--label-right kt-margin-t-20 kt-margin-b-10">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="row align-items-center">
                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
															<span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                    <!--end: Search Form -->
                </div>

                <div class="kt-portlet__body kt-portlet__body--fit">

                    @if(session()->has('success'))
                        <div class="alert alert-success fade show" role="alert">
                            <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
                            <div class="alert-text">{{session()->get('success')}}!</div>
                            <div class="alert-close">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger fade show" role="alert">
                            <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
                            <div class="alert-text">{{session()->get('error')}}!</div>
                            <div class="alert-close">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="la la-close"></i></span>
                                </button>
                            </div>
                        </div>
                        @endif

                                <!--begin: Datatable -->
                        <div class="kt_datatable" id="base_column_width">

                        </div>

                        <!--end: Datatable -->
                </div>
            </div>
        </div>

        <!-- end:: Content -->
    </div>
@stop

@section('script')

    <script>
        /*var data_url = 'https://keenthemes.com/keen/tools/preview/api/datatables/demos/default2.php'*/
        var data_url = '{!! url("admin/employee/lists") !!}';

    </script>

    {{--Datatable--}}
    <script>

        "use strict";
        // Class definition

        var KTDatatableColumnWidthDemo = function() {
            // Private functions

            // basic demo
            var demo = function() {
                var datatable = $('.kt_datatable').KTDatatable({
                    // datasource definition
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                url: data_url,
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: false,
                        serverSorting: true,
                    },

                    // layout definition
                    layout: {
                        scroll: true, // enable/disable datatable scroll both horizontal and
                        // vertical when needed.
                        height: null, // datatable's body's fixed height
                        footer: false, // display/hide footer
                    },

                    // column sorting
                    sortable: true,

                    pagination: true,

                    search: {
                        input: $('#generalSearch'),
                    },

                    // columns definition
                    columns: [
                        {
                            field: 'id',
                            title: 'Sr.No',
                            sortable: 'desc',
                            width: 30,
                            type: 'number',
                            selector: false,
                            textAlign: 'center',
                        }, {
                            field: 'First Name',
                            title: 'First Name',
                        }, {
                            field: 'Last Name',
                            title: 'Last Name',
                        },
                        {
                            field: 'Profile',
                            title: 'Profile',
                        },
                        {
                            field: 'Username',
                            title: 'Username',
                        },
                        {
                            field: 'Email',
                            title: 'Email',
                        },
                        {
                            field: 'Designation',
                            title: 'Designation',
                        },
                        {
                            field: 'Department',
                            title: 'Department',
                        },
                        {
                            field: 'Location',
                            title: 'Location',
                        },
                        {
                            field: 'Roll',
                            title: 'Roll',
                        },
                        {
                            field: 'Status',
                            title: 'Status',
                        },{
                            field: 'created_at',
                            title: 'Date Created',
                        },  {
                            field: 'Actions',
                            title: 'Actions',
                            sortable: false,
                            width: 110,
                            overflow: 'visible',
                            autoHide: false,
                            template: function(row, index, datatable) {
                                /* console.log(row.id) ;
                                 console.log(index) ;
                                 console.log(datatable) ;*/

                                var url='{{ URL::to('admin/service-style/edit/:id')}}';
                                var url1=url.replace(':id',row.id);

                                var url2 = '{{ URL::to('admin/service-style/delete/:id')}}';
                                var url3 = url2.replace(':id', row.id);
                                return '\
							\
							<a href="'+url1+'" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details">\
								<i class="la la-edit"></i>\
							</a>\
							<a href="'+url3+'" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete">\
								<i class="la la-trash"></i>\
							</a>\
						';
                            },
                        }],

                });

                $('#kt_form_status').on('change', function() {
                    datatable.search($(this).val().toLowerCase(), 'status');
                });

                $('#kt_form_type').on('change', function() {
                    datatable.search($(this).val().toLowerCase(), 'type');
                });

                $('#kt_form_status,#kt_form_type').selectpicker();

            };

            return {
                // public functions
                init: function() {
                    demo();
                }
            };
        }();

        jQuery(document).ready(function() {
            KTDatatableColumnWidthDemo.init();
        });

    </script>
@stop
