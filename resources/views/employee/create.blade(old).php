
@extends('layouts.default')
@section('css')


@stop
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid"></div>

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">

            <div class="kt-portlet" >
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h4 class="kt-portlet__head-title">
                            <h5> <strong>{!! trans('messages.add_new') !!}</strong> {!! trans('messages.user') !!}</h5>
                        </h4>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="{!! url('admin/employee/store') !!}" id="employee">
                    <div class="kt-portlet__body">

                        <div class="form-group">
                            <label>{!! trans('messages.email') !!}</label>
                            <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="{!! trans('messages.email') !!}">
                        </div>
                        <div class="form-group">
                            <label>{!! trans('messages.username') !!}</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="{!! trans('messages.username') !!}">
                        </div>
                        <div class="form-group">
                            <label>{!! trans('messages.first').' '.trans('messages.name') !!}</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="{{trans('messages.first').' '.trans('messages.name')}}">
                        </div>
                        <div class="form-group">
                            <label>{!! trans('messages.last').' '.trans('messages.name') !!}</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="{{trans('messages.last').' '.trans('messages.name')}}">
                        </div>
                        <div class="form-group">
                            <label>{!! trans('messages.password')!!}</label>
                            <input type="password" class="form-control text-input @if(config('config.enable_password_strength_meter')) password-strength @endif" name="password" placeholder="{{trans('messages.password')}}">
                        </div>
                        <div class="form-group">
                            <label>{!! trans('messages.confirm').' '.trans('messages.password') !!}</label>
                            <input type="password" class="form-control text-input" name="password_confirmation" placeholder="{{trans('messages.confirm').' '.trans('messages.password')}}">
                        </div>
                        <div class="form-group">
                            <label>{!! trans('messages.role') !!}</label>
                            <select class="form-control" id="role_id">
                                <option>{!! trans('messages.select_one') !!}</option>
                                <option value="1">Arihant</option>
                                <option value="1">Arihant</option>
                                <option value="1">Arihant</option>
                                <option value="1">Arihant</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{!! trans('messages.user').' '.trans('messages.code') !!}</label>
                            <input type="text" class="form-control text-input " name="employee_code" placeholder="{{trans('messages.user').' '.trans('messages.code')}}">
                        </div>
                        <div class="form-group">
                            <label>{!! trans('messages.designation') !!}</label>
                            <select class="form-control" id="designation_id">
                                <option>{!! trans('messages.select_one') !!}</option>
                                <option value="1">Arihant</option>
                                <option value="1">Arihant</option>
                                <option value="1">Arihant</option>
                                <option value="1">Arihant</option>
                            </select>
                        </div>

                        <div class="form-group row">
                            <label for="example-date-input" class="col-2 col-form-label">{!! trans('messages.date_of').' '.trans('messages.joining') !!}</label>
                            <div class="col-10">
                                <input class="form-control" type="date" value="2011-08-19" id="example-date-input">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-date-input" class="col-2 col-form-label">{!! trans('messages.company') !!}</label>
                            <div class="col-10">
                                <select class="form-control" id="location_id">
                                    <option>select company</option>
                                    <option value="1">Arihant</option>
                                    <option value="1">Arihant</option>
                                    <option value="1">Arihant</option>
                                    <option value="1">Arihant</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleSelect1">{!! trans('messages.company') !!}</label>
                            <select class="form-control" id="location_id">
                                <option>select company</option>
                                <option value="1">Arihant</option>
                                <option value="1">Arihant</option>
                                <option value="1">Arihant</option>
                                <option value="1">Arihant</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input name="send_welcome_email" type="checkbox" class="switch-input" data-size="mini" data-on-text="Yes" data-off-text="No" value="1"> {{trans('messages.send')}} welcome email
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-primary">{!! trans('messages.create').' '.trans('messages.account') !!}</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>

        </div>
    </div>
@stop

@section('script')
    <script>
        "use strict";
        // Class definition

        var KTFormControls = function () {
            // Private functions

            var demo1 = function () {
                $( "#service" ).validate({
                    // define validation rules
                    rules: {
                        input: {
                            required: true
                        },

                        option: {
                            required: true
                        },

                    },
                    messages: {
                        input:{
                            required: "Please enter service style",
                        },
                        option: {
                            required: "Please select options",
                        },
                    },

                    //display error alert on form submit
                    invalidHandler: function(event, validator) {
                        var alert = $('#service_msg');
                        alert.parent().removeClass('kt-hidden');
                        KTUtil.scrollTo("service", -200);
                    },

                    submitHandler: function (form) {
                        //form[0].submit(); // submit the form
                        form.submit();
                    }
                });
            }

            return {
                // public functions
                init: function() {
                    demo1();
                }
            };
        }();

        $('#submit').click(function() {
            $("#service").valid();
        });

        jQuery(document).ready(function() {
            KTFormControls.init();
        });
    </script>

@stop


