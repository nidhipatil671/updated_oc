
<html lang="en">

<!-- begin::Head -->

@include('layouts.head')
        <!-- end::Head -->

<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Header Mobile -->
@include('layouts.mobile_header')

        <!-- end:: Header Mobile -->

<!-- begin:: Root -->
<div class="kt-grid kt-grid--hor kt-grid--root">

    <!-- begin:: Page -->
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <!-- begin:: Aside -->
        @include('layouts.sidebar')
                <!-- end:: Aside -->

        <!-- begin:: Wrapper -->
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <!-- begin:: Header -->
            @include('layouts.header')
                    <!-- end:: Header -->
             @yield('content')
            <!-- begin:: Footer -->
            @include('layouts.footer')
                    <!-- end:: Footer -->
        </div>

        <!-- end:: Wrapper -->
    </div>

    <!-- end:: Page -->
</div>


<!-- end:: Quick Panel -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>



<!--begin::Page Scripts(used by this page) -->
@include('layouts.script')

<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>