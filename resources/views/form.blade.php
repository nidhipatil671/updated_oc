<!DOCTYPE html>

<!--
Theme: Keen - The Ultimate Bootstrap Admin Theme
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: You must have a valid license purchased only from https://themes.getbootstrap.com/product/keen-the-ultimate-bootstrap-admin-theme/ in order to legally use the theme for your project.
-->
<html lang="en">

<!-- begin::Head -->
@include('layouts.head')

<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Header Mobile -->
@include('layouts.mobile_header')
<!-- end:: Header Mobile -->

<!-- begin:: Root -->
<div class="kt-grid kt-grid--hor kt-grid--root">

    <!-- begin:: Page -->
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <!-- begin:: Aside -->
        @include('layouts.sidebar')
        <!-- end:: Aside -->

        <!-- begin:: Wrapper -->
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <!-- begin:: Header -->
            @include('layouts.header')

            <!-- end:: Header -->
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Subheader -->
                <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-subheader__main">
                            <h3 class="kt-subheader__title">Form Controls</h3>
                            <span class="kt-subheader__separator kt-hidden"></span>
                            <div class="kt-subheader__breadcrumbs">
                                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                                <span class="kt-subheader__breadcrumbs-separator"></span>
                                <a href="" class="kt-subheader__breadcrumbs-link">
                                    Extended </a>
                                <span class="kt-subheader__breadcrumbs-separator"></span>
                                <a href="" class="kt-subheader__breadcrumbs-link">
                                    Forms </a>
                                <span class="kt-subheader__breadcrumbs-separator"></span>
                                <a href="" class="kt-subheader__breadcrumbs-link">
                                    Validation </a>
                                <span class="kt-subheader__breadcrumbs-separator"></span>
                                <a href="" class="kt-subheader__breadcrumbs-link">
                                    Controls </a>
                            </div>
                        </div>
                        <div class="kt-subheader__toolbar">
                            <div class="kt-subheader__wrapper">
                                <a href="#" class="btn btn-icon btn btn-label btn-label-brand btn-bold" data-toggle="kt-tooltip" title="Reports" data-placement="top"><i class="flaticon2-writing"></i></a>
                                <a href="#" class="btn btn-icon btn btn-label btn-label-brand btn-bold" data-toggle="kt-tooltip" title="Calendar" data-placement="top"><i class="flaticon2-hourglass-1"></i></a>
                                <div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions" data-placement="top">
                                    <a href="#" class="btn btn-icon btn btn-label btn-label-brand btn-bold" data-toggle="dropdown" data-offset="0px,0px" aria-haspopup="true" aria-expanded="false">
                                        <i class="flaticon2-add-1"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                        <ul class="kt-nav kt-nav--active-bg" role="tablist">
                                            <li class="kt-nav__item">
                                                <a href="" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-psd"></i>
                                                    <span class="kt-nav__link-text">Document</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link" role="tab">
                                                    <i class="kt-nav__link-icon flaticon2-supermarket"></i>
                                                    <span class="kt-nav__link-text">Message</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-shopping-cart"></i>
                                                    <span class="kt-nav__link-text">Product</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link" role="tab">
                                                    <i class="kt-nav__link-icon flaticon2-chart2"></i>
                                                    <span class="kt-nav__link-text">Report</span>
															<span class="kt-nav__link-badge">
																<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--rounded">pdf</span>
															</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-sms"></i>
                                                    <span class="kt-nav__link-text">Post</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-avatar"></i>
                                                    <span class="kt-nav__link-text">Customer</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions" data-placement="top">
                                    <a href="#" class="btn btn btn-label btn-label-brand btn-bold" data-toggle="dropdown" data-offset="0 0" aria-haspopup="true" aria-expanded="false">
                                        Reports
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav kt-nav--active-bg" id="kt_nav_1" role="tablist">
                                            <li class="kt-nav__item">
                                                <a href="" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-psd"></i>
                                                    <span class="kt-nav__link-text">Products</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link" role="tab" id="kt_nav_link_1">
                                                    <i class="kt-nav__link-icon flaticon2-supermarket"></i>
                                                    <span class="kt-nav__link-text">Customers</span>
															<span class="kt-nav__link-badge">
																<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--rounded">6</span>
															</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-shopping-cart"></i>
                                                    <span class="kt-nav__link-text">Orders</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-shopping-cart"></i>
                                                    <span class="kt-nav__link-text">Reports</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link" role="tab" id="kt_nav_link_2">
                                                    <i class="kt-nav__link-icon flaticon2-chart2"></i>
                                                    <span class="kt-nav__link-text">Console</span>
															<span class="kt-nav__link-badge">
																<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--rounded">new</span>
															</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-sms"></i>
                                                    <span class="kt-nav__link-text">Settings</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end:: Subheader -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                    <!--begin::Portlet-->
                    <div class="row">
                        <div class="col-lg-6">

                            <!--begin::Portlet-->
                            <div class="kt-portlet">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">Default Validation</h3>
                                    </div>
                                </div>

                                <!--begin::Form-->
                                <form class="kt-form kt-form--label-right" id="kt_form_1">
                                    <div class="kt-portlet__body">
                                        <div class="form-group form-group-last kt-hide">
                                            <div class="alert alert-danger" role="alert" id="kt_form_1_msg">
                                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                                <div class="alert-text">
                                                    Oh snap! Change a few things up and try submitting again.
                                                </div>
                                                <div class="alert-close">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="la la-close"></i></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Email *</label>
                                            <div class="col-lg-9 col-md-9 col-sm-12">
                                                <input type="text" class="form-control" name="email" placeholder="Enter your email">
                                                <span class="form-text text-muted">We'll never share your email with anyone else.</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">URL *</label>
                                            <div class="col-lg-9 col-md-9 col-sm-12">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="url" placeholder="Enter your url">
                                                    <div class="input-group-append"><span class="input-group-text">.via.com</span></div>
                                                </div>
                                                <span class="form-text text-muted">Please enter your website URL.</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Digits</label>
                                            <div class="col-lg-9 col-md-9 col-sm-12">
                                                <div class="kt-input-icon kt-input-icon--left">
                                                    <input type="text" class="form-control" name="digits" placeholder="Enter digits">
                                                    <span class="kt-input-icon__icon kt-input-icon__icon--left"><span><i class="la la-calculator"></i></span></span>
                                                </div>
                                                <span class="form-text text-muted">Please enter only digits</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Credit Card</label>
                                            <div class="col-lg-9 col-md-9 col-sm-12">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="creditcard" placeholder="Enter card number">
                                                    <div class="input-group-append"><span class="input-group-text"><i class="la la-credit-card"></i></span></div>
                                                </div>
                                                <span class="form-text text-muted">Please enter your credit card number</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">US Phone</label>
                                            <div class="col-lg-9 col-md-9 col-sm-12">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="phone" placeholder="Enter phone">
                                                    <div class="input-group-append"><a href="#" class="btn btn-brand btn-icon"><i class="la la-phone"></i></a></div>
                                                </div>
                                                <span class="form-text text-muted">Please enter your US phone number</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Option *</label>
                                            <div class="col-lg-9 col-md-9 col-sm-12 form-group-sub">
                                                <select class="form-control" name="option">
                                                    <option value="">Select</option>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                                <span class="form-text text-muted">Please select an option.</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Options *</label>
                                            <div class="col-lg-9 col-md-9 col-sm-12 form-group-sub">
                                                <select class="form-control" name="options" multiple size="5">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                                <span class="form-text text-muted">Please select at least one or maximum 4 options</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Memo *</label>
                                            <div class="col-lg-9 col-md-9 col-sm-12">
                                                <textarea class="form-control" name="memo" placeholder="Enter a menu" rows="8"></textarea>
                                                <span class="form-text text-muted">Please enter a menu within text length range 10 and 100.</span>
                                            </div>
                                        </div>
                                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-xl"></div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Checkbox *</label>
                                            <div class="col-lg-9 col-md-9 col-sm-12">
                                                <div class="kt-checkbox-inline">
                                                    <label class="kt-checkbox">
                                                        <input type="checkbox" name="checkbox"> Tick me
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <span class="form-text text-muted">Please tick the checkbox</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Checkboxes *</label>
                                            <div class="col-lg-9 col-md-9 col-sm-12">
                                                <div class="kt-checkbox-list">
                                                    <label class="kt-checkbox">
                                                        <input type="checkbox" name="checkboxes"> Option 1
                                                        <span></span>
                                                    </label>
                                                    <label class="kt-checkbox">
                                                        <input type="checkbox" name="checkboxes"> Option 2
                                                        <span></span>
                                                    </label>
                                                    <label class="kt-checkbox">
                                                        <input type="checkbox" name="checkboxes"> Option 3
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <span class="form-text text-muted">Please select at lease 1 and maximum 2 options</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Radios *</label>
                                            <div class="col-lg-9 col-md-9 col-sm-12">
                                                <div class="kt-radio-inline">
                                                    <label class="kt-radio">
                                                        <input type="checkbox" name="radio"> Option 1
                                                        <span></span>
                                                    </label>
                                                    <label class="kt-radio">
                                                        <input type="checkbox" name="radio"> Option 2
                                                        <span></span>
                                                    </label>
                                                    <label class="kt-radio">
                                                        <input type="radio" name="radio"> Option 3
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <span class="form-text text-muted">Please select an option</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-lg-9 ml-lg-auto">
                                                    <button type="submit" class="btn btn-accent">Validate</button>
                                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <!--end::Form-->
                            </div>

                            <!--end::Portlet-->
                        </div>
                        <div class="col-lg-6">
                            <div class="kt-portlet">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">Advanced Validation</h3>
                                    </div>
                                </div>

                                <!--begin::Form-->
                                <form class="kt-form kt-form--label-right" id="kt_form_2">
                                    <div class="kt-portlet__body">
                                        <div class="kt-section">
                                            <h3 class="kt-section__title">
                                                Billing Information:
                                            </h3>
                                            <div class="kt-section__content">
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <label class="form-control-label">* Cardholder Name:</label>
                                                        <input type="text" name="billing_card_name" class="form-control" placeholder="" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <label class="form-control-label">* Card Number:</label>
                                                        <input type="text" name="billing_card_number" class="form-control" placeholder="" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-last row">
                                                    <div class="col-lg-4 form-group-sub">
                                                        <label class="form-control-label">* Exp Month:</label>
                                                        <select class="form-control" name="billing_card_exp_month">
                                                            <option value="">Select</option>
                                                            <option value="01">01</option>
                                                            <option value="02">02</option>
                                                            <option value="03">03</option>
                                                            <option value="04">04</option>
                                                            <option value="05">05</option>
                                                            <option value="06">06</option>
                                                            <option value="07">07</option>
                                                            <option value="08">08</option>
                                                            <option value="09">09</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4 form-group-sub">
                                                        <label class="form-control-label">* Exp Year:</label>
                                                        <select class="form-control" name="billing_card_exp_year">
                                                            <option value="">Select</option>
                                                            <option value="2018">2018</option>
                                                            <option value="2019">2019</option>
                                                            <option value="2020">2020</option>
                                                            <option value="2021">2021</option>
                                                            <option value="2022">2022</option>
                                                            <option value="2023">2023</option>
                                                            <option value="2024">2024</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4 form-group-sub">
                                                        <label class="form-control-label">* CVV:</label>
                                                        <input type="number" class="form-control" name="billing_card_cvv" placeholder="" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-xl"></div>
                                        <div class="kt-section">
                                            <h3 class="kt-section__title">
                                                Billing Address
                                                <i data-toggle="kt-tooltip" data-width="auto" class="kt-section__help" title="If different than the corresponding address"></i>
                                            </h3>
                                            <div class="kt-section__content">
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <label class="form-control-label">* Address 1:</label>
                                                        <input type="text" name="billing_address_1" class="form-control" placeholder="" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <label class="form-control-label">Address 2:</label>
                                                        <input type="text" name="billing_address_2" class="form-control" placeholder="" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-last row">
                                                    <div class="col-lg-5 form-group-sub">
                                                        <label class="form-control-label">* City:</label>
                                                        <input type="text" class="form-control" name="billing_city" placeholder="" value="">
                                                    </div>
                                                    <div class="col-lg-5 form-group-sub">
                                                        <label class="form-control-label">* State:</label>
                                                        <input type="text" class="form-control" name="billing_state" placeholder="" value="">
                                                    </div>
                                                    <div class="col-lg-2 form-group-sub">
                                                        <label class="form-control-label">* ZIP:</label>
                                                        <input type="text" class="form-control" name="billing_zip" placeholder="" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-xl"></div>
                                        <div class="kt-section">
                                            <h3 class="kt-section__title kt-margin-b-20">
                                                Delivery Type:
                                            </h3>
                                            <div class="kt-section__content">
                                                <div class="form-group form-group-last">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <label class="kt-option">
																		<span class="kt-option__control">
																			<span class="kt-radio kt-radio--state-brand">
																				<input type="radio" name="billing_delivery" value="">
																				<span></span>
																			</span>
																		</span>
																		<span class="kt-option__label">
																			<span class="kt-option__head">
																				<span class="kt-option__title">
																					Standart Delevery
																				</span>
																				<span class="kt-option__focus">
																					Free
																				</span>
																			</span>
																			<span class="kt-option__body">
																				Estimated 14-20 Day Shipping
																				(&nbsp;Duties end taxes may be due
																				upon delivery&nbsp;)
																			</span>
																		</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="kt-option">
																		<span class="kt-option__control">
																			<span class="kt-radio kt-radio--state-brand">
																				<input type="radio" name="billing_delivery" value="">
																				<span></span>
																			</span>
																		</span>
																		<span class="kt-option__label">
																			<span class="kt-option__head">
																				<span class="kt-option__title">
																					Fast Delevery
																				</span>
																				<span class="kt-option__focus">
																					$&nbsp;8.00
																				</span>
																			</span>
																			<span class="kt-option__body">
																				Estimated 2-5 Day Shipping
																				(&nbsp;Duties end taxes may be due
																				upon delivery&nbsp;)
																			</span>
																		</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-text text-muted">

                                                        <!--must use this helper element to display error message for the options-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <button type="submit" class="btn btn-accent">Validate</button>
                                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <!--end::Form-->
                            </div>

                            <!--end::Portlet-->
                        </div>
                    </div>
                </div>

                <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            @include('layouts.footer')
            <!-- end:: Footer -->
        </div>

        <!-- end:: Wrapper -->
    </div>

    <!-- end:: Page -->
</div>

<!-- end:: Root -->

<!-- begin:: Topbar Offcanvas Panels -->

<!-- begin::Offcanvas Toolbar Quick Actions -->
<div id="kt_offcanvas_toolbar_quick_actions" class="kt-offcanvas-panel">
    <div class="kt-offcanvas-panel__head">
        <h3 class="kt-offcanvas-panel__title">
            Quick Actions
        </h3>
        <a href="#" class="kt-offcanvas-panel__close" id="kt_offcanvas_toolbar_quick_actions_close"><i class="flaticon2-delete"></i></a>
    </div>
    <div class="kt-offcanvas-panel__body">
        <div class="kt-grid-nav-v2">
            <a href="#" class="kt-grid-nav-v2__item">
                <div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-box"></i></div>
                <div class="kt-grid-nav-v2__item-title">Orders</div>
            </a>
            <a href="#" class="kt-grid-nav-v2__item">
                <div class="kt-grid-nav-v2__item-icon"><i class="flaticon-download-1"></i></div>
                <div class="kt-grid-nav-v2__item-title">Uploades</div>
            </a>
            <a href="#" class="kt-grid-nav-v2__item">
                <div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-supermarket"></i></div>
                <div class="kt-grid-nav-v2__item-title">Products</div>
            </a>
            <a href="#" class="kt-grid-nav-v2__item">
                <div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-avatar"></i></div>
                <div class="kt-grid-nav-v2__item-title">Customers</div>
            </a>
            <a href="#" class="kt-grid-nav-v2__item">
                <div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-list"></i></div>
                <div class="kt-grid-nav-v2__item-title">Blog Posts</div>
            </a>
            <a href="#" class="kt-grid-nav-v2__item">
                <div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-settings"></i></div>
                <div class="kt-grid-nav-v2__item-title">Settings</div>
            </a>
        </div>
    </div>
</div>

<!-- end::Offcanvas Toolbar Quick Actions -->

<!-- end:: Topbar Offcanvas Panels -->

<!-- begin:: Quick Panel -->
<div id="kt_quick_panel" class="kt-offcanvas-panel">
    <div class="kt-offcanvas-panel__nav">
        <ul class="nav nav-pills" role="tablist">
            <li class="nav-item active">
                <a class="nav-link active" data-toggle="tab" href="#kt_quick_panel_tab_notifications" role="tab">Notifications</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_actions" role="tab">Actions</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_settings" role="tab">Settings</a>
            </li>
        </ul>
        <button class="kt-offcanvas-panel__close" id="kt_quick_panel_close_btn"><i class="flaticon2-delete"></i></button>
    </div>
    <div class="kt-offcanvas-panel__body">
        <div class="tab-content">
            <div class="tab-pane fade show kt-offcanvas-panel__content kt-scroll active" id="kt_quick_panel_tab_notifications" role="tabpanel">

                <!--Begin::Timeline -->
                <div class="kt-timeline">

                    <!--Begin::Item -->
                    <div class="kt-timeline__item kt-timeline__item--success">
                        <div class="kt-timeline__item-section">
                            <div class="kt-timeline__item-section-border">
                                <div class="kt-timeline__item-section-icon">
                                    <i class="flaticon-feed kt-font-success"></i>
                                </div>
                            </div>
                            <span class="kt-timeline__item-datetime">02:30 PM</span>
                        </div>
                        <a href="" class="kt-timeline__item-text">
                            KeenThemes created new layout whith tens of new options for Keen Admin panel
                        </a>
                        <div class="kt-timeline__item-info">
                            HTML,CSS,VueJS
                        </div>
                    </div>

                    <!--End::Item -->

                    <!--Begin::Item -->
                    <div class="kt-timeline__item kt-timeline__item--danger">
                        <div class="kt-timeline__item-section">
                            <div class="kt-timeline__item-section-border">
                                <div class="kt-timeline__item-section-icon">
                                    <i class="flaticon-safe-shield-protection kt-font-danger"></i>
                                </div>
                            </div>
                            <span class="kt-timeline__item-datetime">01:20 AM</span>
                        </div>
                        <a href="" class="kt-timeline__item-text">
                            New secyrity alert by Firewall & order to take aktion on User Preferences
                        </a>
                        <div class="kt-timeline__item-info">
                            Security, Fieewall
                        </div>
                    </div>

                    <!--End::Item -->

                    <!--Begin::Item -->
                    <div class="kt-timeline__item kt-timeline__item--brand">
                        <div class="kt-timeline__item-section">
                            <div class="kt-timeline__item-section-border">
                                <div class="kt-timeline__item-section-icon">
                                    <i class="flaticon2-box kt-font-brand"></i>
                                </div>
                            </div>
                            <span class="kt-timeline__item-datetime">Yestardey</span>
                        </div>
                        <a href="" class="kt-timeline__item-text">
                            FlyMore design mock-ups been uploadet by designers Bob, Naomi, Richard
                        </a>
                        <div class="kt-timeline__item-info">
                            PSD, Sketch, AJ
                        </div>
                    </div>

                    <!--End::Item -->

                    <!--Begin::Item -->
                    <div class="kt-timeline__item kt-timeline__item--warning">
                        <div class="kt-timeline__item-section">
                            <div class="kt-timeline__item-section-border">
                                <div class="kt-timeline__item-section-icon">
                                    <i class="flaticon-pie-chart-1 kt-font-warning"></i>
                                </div>
                            </div>
                            <span class="kt-timeline__item-datetime">Aug 13,2018</span>
                        </div>
                        <a href="" class="kt-timeline__item-text">
                            Meeting with Ken Digital Corp ot Unit14, 3 Edigor Buildings, George Street, Loondon<br> England, BA12FJ
                        </a>
                        <div class="kt-timeline__item-info">
                            Meeting, Customer
                        </div>
                    </div>

                    <!--End::Item -->

                    <!--Begin::Item -->
                    <div class="kt-timeline__item kt-timeline__item--info">
                        <div class="kt-timeline__item-section">
                            <div class="kt-timeline__item-section-border">
                                <div class="kt-timeline__item-section-icon">
                                    <i class="flaticon-notepad kt-font-info"></i>
                                </div>
                            </div>
                            <span class="kt-timeline__item-datetime">May 09, 2018</span>
                        </div>
                        <a href="" class="kt-timeline__item-text">
                            KeenThemes created new layout whith tens of new options for Keen Admin panel
                        </a>
                        <div class="kt-timeline__item-info">
                            HTML,CSS,VueJS
                        </div>
                    </div>

                    <!--End::Item -->

                    <!--Begin::Item -->
                    <div class="kt-timeline__item kt-timeline__item--accent">
                        <div class="kt-timeline__item-section">
                            <div class="kt-timeline__item-section-border">
                                <div class="kt-timeline__item-section-icon">
                                    <i class="flaticon-bell kt-font-success"></i>
                                </div>
                            </div>
                            <span class="kt-timeline__item-datetime">01:20 AM</span>
                        </div>
                        <a href="" class="kt-timeline__item-text">
                            New secyrity alert by Firewall & order to take aktion on User Preferences
                        </a>
                        <div class="kt-timeline__item-info">
                            Security, Fieewall
                        </div>
                    </div>

                    <!--End::Item -->
                </div>

                <!--End::Timeline -->
            </div>
            <div class="tab-pane fade kt-offcanvas-panel__content kt-scroll" id="kt_quick_panel_tab_actions" role="tabpanel">

                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--solid-success">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon kt-hide"><i class="flaticon-stopwatch"></i></span>
                            <h3 class="kt-portlet__head-title">Recent Bills</h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-group">
                                <div class="dropdown dropdown-inline">
                                    <button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <i class="flaticon-more"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Separated link</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-portlet__content">
                            Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry.
                        </div>
                    </div>
                    <div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
                        <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
                        <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
                    </div>
                </div>

                <!--end::Portlet-->

                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--solid-focus">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon kt-hide"><i class="flaticon-stopwatch"></i></span>
                            <h3 class="kt-portlet__head-title">Latest Orders</h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-group">
                                <div class="dropdown dropdown-inline">
                                    <button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <i class="flaticon-more"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Separated link</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-portlet__content">
                            Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry.
                        </div>
                    </div>
                    <div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
                        <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
                        <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
                    </div>
                </div>

                <!--end::Portlet-->

                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--solid-info">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Latest Invoices</h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-group">
                                <div class="dropdown dropdown-inline">
                                    <button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <i class="flaticon-more"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Separated link</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-portlet__content">
                            Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry.
                        </div>
                    </div>
                    <div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
                        <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
                        <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
                    </div>
                </div>

                <!--end::Portlet-->

                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--solid-warning">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">New Comments</h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-group">
                                <div class="dropdown dropdown-inline">
                                    <button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <i class="flaticon-more"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Separated link</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-portlet__content">
                            Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry.
                        </div>
                    </div>
                    <div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
                        <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
                        <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
                    </div>
                </div>

                <!--end::Portlet-->

                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--solid-brand">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Recent Posts</h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-group">
                                <div class="dropdown dropdown-inline">
                                    <button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <i class="flaticon-more"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Separated link</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-portlet__content">
                            Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry.
                        </div>
                    </div>
                    <div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
                        <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
                        <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
                    </div>
                </div>

                <!--end::Portlet-->
            </div>
            <div class="tab-pane fade kt-offcanvas-panel__content kt-scroll" id="kt_quick_panel_tab_settings" role="tabpanel">
                <form class="kt-form">
                    <div class="kt-heading kt-heading--space-sm">Notifications</div>
                    <div class="form-group form-group-xs row">
                        <label class="col-8 col-form-label">Enable notifications:</label>
                        <div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm">
										<label>
                                            <input type="checkbox" checked="checked" name="quick_panel_notifications_1">
                                            <span></span>
                                        </label>
									</span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-8 col-form-label">Enable audit log:</label>
                        <div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm">
										<label>
                                            <input type="checkbox" name="quick_panel_notifications_2">
                                            <span></span>
                                        </label>
									</span>
                        </div>
                    </div>
                    <div class="form-group form-group-last form-group-xs row">
                        <label class="col-8 col-form-label">Notify on new orders:</label>
                        <div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm">
										<label>
                                            <input type="checkbox" checked="checked" name="quick_panel_notifications_2">
                                            <span></span>
                                        </label>
									</span>
                        </div>
                    </div>
                    <div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>
                    <div class="kt-heading kt-heading--space-sm">Orders</div>
                    <div class="form-group form-group-xs row">
                        <label class="col-8 col-form-label">Enable order tracking:</label>
                        <div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--danger">
										<label>
                                            <input type="checkbox" checked="checked" name="quick_panel_notifications_3">
                                            <span></span>
                                        </label>
									</span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-8 col-form-label">Enable orders reports:</label>
                        <div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--danger">
										<label>
                                            <input type="checkbox" name="quick_panel_notifications_3">
                                            <span></span>
                                        </label>
									</span>
                        </div>
                    </div>
                    <div class="form-group form-group-last form-group-xs row">
                        <label class="col-8 col-form-label">Allow order status auto update:</label>
                        <div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--danger">
										<label>
                                            <input type="checkbox" checked="checked" name="quick_panel_notifications_4">
                                            <span></span>
                                        </label>
									</span>
                        </div>
                    </div>
                    <div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>
                    <div class="kt-heading kt-heading--space-sm">Customers</div>
                    <div class="form-group form-group-xs row">
                        <label class="col-8 col-form-label">Enable customer singup:</label>
                        <div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--success">
										<label>
                                            <input type="checkbox" checked="checked" name="quick_panel_notifications_5">
                                            <span></span>
                                        </label>
									</span>
                        </div>
                    </div>
                    <div class="form-group form-group-xs row">
                        <label class="col-8 col-form-label">Enable customers reporting:</label>
                        <div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--success">
										<label>
                                            <input type="checkbox" name="quick_panel_notifications_5">
                                            <span></span>
                                        </label>
									</span>
                        </div>
                    </div>
                    <div class="form-group form-group-last form-group-xs row">
                        <label class="col-8 col-form-label">Notifiy on new customer registration:</label>
                        <div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--success">
										<label>
                                            <input type="checkbox" checked="checked" name="quick_panel_notifications_6">
                                            <span></span>
                                        </label>
									</span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- end:: Quick Panel -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->

<!-- begin:: Demo Toolbar -->
<ul class="kt-sticky-toolbar" style="margin-top: 30px;">
    <li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--demo-toggle" id="kt_demo_panel_toggle" data-toggle="kt-tooltip" title="Check out more demos" data-placement="right">
        <a href="#" class="">Demos</a>
    </li>
    <li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--builder" data-toggle="kt-tooltip" title="Layout Builder" data-placement="left">
        <a href="https://keenthemes.com/keen/preview/demo1/builder.html" target="_blank"><i class="flaticon2-box"></i></a>
    </li>
    <li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--docs" data-toggle="kt-tooltip" title="Documentation" data-placement="left">
        <a href="https://keenthemes.com/keen/?page=docs" target="_blank"><i class="flaticon2-file"></i></a>
    </li>
</ul>

<!-- end:: Demo Toolbar -->

<!-- begin::Demo Panel -->
<div id="kt_demo_panel" class="kt-demo-panel">
    <div class="kt-demo-panel__head">
        <h3 class="kt-demo-panel__title">
            Select A Demo

            <!--<small>5</small>-->
        </h3>
        <a href="#" class="kt-demo-panel__close" id="kt_demo_panel_close"><i class="flaticon2-delete"></i></a>
    </div>
    <div class="kt-demo-panel__body">
        <div class="kt-demo-panel__item kt-demo-panel__item--active">
            <div class="kt-demo-panel__item-title">
                Demo 1
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos/demo2.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="https://keenthemes.com/keen/preview/demo1/index.html" class="btn btn-brand btn-elevate " target="_blank">Default</a>
                    <a href="https://keenthemes.com/keen/preview/demo1/rtl/index.html" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 2
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos/demo3.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="https://keenthemes.com/keen/preview/demo2/index.html" class="btn btn-brand btn-elevate " target="_blank">Default</a>
                    <a href="https://keenthemes.com/keen/preview/demo2/rtl/index.html" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 3
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos/demo1-1.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="https://keenthemes.com/keen/preview/demo3/index.html" class="btn btn-brand btn-elevate " target="_blank">Default</a>
                    <a href="https://keenthemes.com/keen/preview/demo3/rtl/index.html" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 4
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos/demo4.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="https://keenthemes.com/keen/preview/demo4/index.html" class="btn btn-brand btn-elevate " target="_blank">Default</a>
                    <a href="https://keenthemes.com/keen/preview/demo4/rtl/index.html" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 5
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos/demo5.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="https://keenthemes.com/keen/preview/demo5/index.html" class="btn btn-brand btn-elevate " target="_blank">Default</a>
                    <a href="https://keenthemes.com/keen/preview/demo5/rtl/index.html" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 6
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos/demo6.jpg" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="https://keenthemes.com/keen/preview/demo6/index.html" class="btn btn-brand btn-elevate " target="_blank">Default</a>
                    <a href="https://keenthemes.com/keen/preview/demo6/rtl/index.html" class="btn btn-light btn-elevate" target="_blank">RTL Version</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 7
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos-small/Demo-1.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 8
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos-small/Demo-2.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 9
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos-small/Demo-3.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 10
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos-small/Demo-4.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 11
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos-small/Demo-5.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 12
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos-small/Demo-6.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 13
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos-small/Demo-7.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 14
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos-small/Demo-8.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 15
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos-small/Demo-9.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 16
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos-small/Demo-10.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 17
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos-small/Demo-11.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 18
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos-small/Demo-12.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 19
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos-small/Demo-14.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
                </div>
            </div>
        </div>
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 20
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="assets/../../../../doc/assets/img/demos-small/Demo-13.png" alt="" />
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled">Coming soon</a>
                </div>
            </div>
        </div>
        <a href="https://themes.getbootstrap.com/product/keen-the-ultimate-bootstrap-admin-theme/" target="_blank" class="kt-demo-panel__purchase btn btn-brand btn-elevate btn-bold btn-upper">
            Buy Keen Now!
        </a>
    </div>
</div>

<!-- end::Demo Panel -->


@include('layouts.script')
<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>